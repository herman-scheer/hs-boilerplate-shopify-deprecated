import gulp from 'gulp'
import plugins from 'gulp-load-plugins'
import config from '../config/assets/default'
import { union } from 'lodash'

const $ = plugins()

gulp.task('modernizr', () => {
  return gulp.src(union(config.client.js, config.client.lib.js, config.client.css))
    .pipe($.modernizr({
      options: [
        'setClasses',
        'addTest',
        'html5printshiv',
        'testProp',
        'fnBind'
      ]
    }))
    .pipe(config.minify ? $.uglify() : $.util.noop())
    .pipe(gulp.dest('public/assets'))
})
