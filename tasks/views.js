import gulp from 'gulp'
import plugins from 'gulp-load-plugins'
import config from '../config/assets/default'
import browserSync from 'browser-sync'

const reload = browserSync.reload


const $ = plugins()

gulp.task('views', () => {
  return gulp.src(config.client.views)
    .pipe($.changed('./public/', {
      extension: '.liquid'
    }))
    .pipe($.cached('jade'))
    .pipe($.filter((file) => {
      return !/\/_/.test(file.path) && !/^_/.test(file.relative);
    }))
    .pipe($.jade({
      pretty: !config.minify,
      force: true
    }))
    .on('error', $.notify.onError())
    .pipe($.rename((file) => {
      const f = file
      f.dirname = f.dirname.replace(/\w+\/client\/views/g, '')
      f.extname = '.liquid'
      return f
    }))
    .pipe(gulp.dest('public/'))
    .pipe(reload({
      stream: true,
      once: true
    }))
})
