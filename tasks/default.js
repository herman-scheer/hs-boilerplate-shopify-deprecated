import gulp from 'gulp'
import runSequence from 'run-sequence'

gulp.task('build', (done) => {
  if (process.env.NODE_ENV === 'development') {
    runSequence('delete', 'images', 'fonts', 'icons', 'copy', ['styles', 'scripts', 'modernizr', 'views'], 'shopify', ['browser-sync', 'watch'], done)
  } else {
    runSequence('delete', 'images', 'fonts', 'icons', 'copy', ['styles', 'scripts', 'modernizr', 'views'], 'shopify', done)
  }
})


gulp.task('default', (done) => {
  runSequence(['browser-sync', 'watch'], done)
})
