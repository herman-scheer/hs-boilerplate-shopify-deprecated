import gulp from 'gulp'
import browserSync from 'browser-sync'

gulp.task('browser-sync', () => {
  browserSync.init(null, {
    proxy: `https://${process.env.SHOPIFY_URL}`,
    injectChanges: false,
    ghostMode: false,
    open: true,
    reloadDelay: 2500,
    notify: false
  })
})
