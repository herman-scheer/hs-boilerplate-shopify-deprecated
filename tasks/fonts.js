import gulp from 'gulp'
import config from '../config/assets/default'

gulp.task('fonts-vendor', () => {
  return gulp.src(config.client.lib.fonts)
    .pipe(gulp.dest('./public/assets'))
})


gulp.task('fonts', ['fonts-vendor'])
