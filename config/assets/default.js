export default {
  minify: process.env.NODE_ENV === 'production',
  client: {
    lib: {
      sass: [
        './node_modules/foundation-sites/scss',
        './node_modules/slick-carousel/slick'
      ],
      fonts: [
        './node_modules/slick-carousel/slick/fonts/**'
      ],
      images: ['./node_modules/slick-carousel/slick/ajax-loader.gif'],
      js: ['./node_modules/foundation-sites/js/foundation.js']
    },
    sassCore: ['modules/client/main.sass'],
    sass: [
      'modules/client/main.sass',
      'modules/client/*/{scss,sass}/**/*.{sass,scss}',
      'modules/client/**/*.{sass,scss}'
    ],
    sassLint: [
      'modules/client/main.sass',
      'modules/client/core/sass/**/*.*',
      'modules/client/**/*.sass',
      'modules/client/**/*.client.module.sass',
      '!modules/client/sass/vendors/**/*.{scss,sass}',
      '!modules/client/sass/utils/**/*.{scss,sass}'
    ],
    css: ['public/assets/stylesheets/main.css'],
    js: [
      'modules/client/main.js',
      'modules/client/app/init.js',
      'modules/*/*.js',
      'modules/*/**/*.js'
    ],
    jsCore: ['modules/client/main.js'],
    views: ['modules/client/views/**/*.jade'],
    images: ['modules/client/img/**/*.{png,jpg,gif,jpeg,ico}'],
    icons: {
      color: ['modules/client/*/icons/color/**/*.svg'],
      noColor: ['modules/client/*/icons/no-color/**/*.svg']
    }
  },
  server: {
    allJS: ['gulpfile.babel.js', 'server.js', 'config/**/*.js', 'modules/server/**/*.js']
  }
}
