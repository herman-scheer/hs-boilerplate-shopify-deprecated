import gulp from 'gulp'

gulp.task('copy', () => {
  return gulp.src('./config/env/**/*.json')
    .pipe(gulp.dest('./public/config'))
})
