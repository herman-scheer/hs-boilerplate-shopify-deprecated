import gulp from 'gulp'
import plugins from 'gulp-load-plugins'
import browserSync from 'browser-sync'

const $ = plugins()
const reload = browserSync.reload

const shopify = {
  api_key: process.env.SHOPIFY_API_KEY,
  password: process.env.SHOPIFY_PASSWORD,
  url: process.env.SHOPIFY_URL,
  theme_id: null
}

const options = {
  basePath: 'public/'
}

gulp.task('shopify', () => {
  return gulp.src('./public/+(assets|layout|config|snippets|templates|locales)/**')
    .pipe($.shopifyUpload(
      shopify.api_key,
      shopify.password,
      shopify.url,
      null,
      options
    ))
})

gulp.task('watch-shopify', () => {
  return $.watch('./public/+(assets|layout|config|snippets|templates|locales)/**')
    .pipe($.shopifyUpload(
      shopify.api_key,
      shopify.password,
      shopify.url,
      null,
      options
    ))
    .pipe(reload({
      stream: true,
      once: true
    }))
})
