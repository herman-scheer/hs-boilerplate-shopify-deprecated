import gulp from 'gulp'
import config from '../config/assets/default'

gulp.task('watch', ['watchify'], () => {
  // client
  gulp.watch(config.client.sass, ['styles'])
  gulp.watch(config.client.icons.noColor, ['icons-no-color'])
  gulp.watch(config.client.icons.color, ['icons-color'])
  gulp.watch(config.client.images, ['images-watch'])
  gulp.watch(config.client.views, ['views'])
  gulp.watch('./config/env/**/*.json', ['copy'])

  gulp.start('watch-shopify')
})
