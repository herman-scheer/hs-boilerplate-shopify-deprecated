import gulp from 'gulp'
import plugins from 'gulp-load-plugins'
import config from '../config/assets/default'
import browserSync from 'browser-sync'

// PostCSS
import postcssNext from 'postcss-cssnext'
import postcssShort from 'postcss-short'

const reload = browserSync.reload

const $ = plugins()

function replaceWithAssetURL(match, imgurl) {
  return `background: url({{ ${imgurl} | asset_url }})`
}

gulp.task('sasslint', () => {
  return gulp.src(config.client.sassLint)
  .pipe($.sassLint())
  .pipe($.sassLint.format())
  .pipe($.sassLint.failOnError())
})

gulp.task('styles', ['sasslink'], () => {
  return gulp.src(config.client.sassCore)
    .pipe(!config.minify ? $.sourcemaps.init() : $.util.noop())
    .pipe($.sass({
      indentedSyntax: true,
      includePaths: config.client.lib.sass
    }))
    .on('error', $.notify.onError())
    .pipe($.postcss([
      // # shorthand property functions (size: 48px > width: 48; height: 48px)
      postcssShort,
      // # css4 magic
      // # http://cssnext.io/features/
      postcssNext({
        browsers: ['last 2 versions', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'],
        features: {
          customProperties: false,
          rem: config.minify
        }
      })
    ]))
    .pipe(!config.minify ? $.sourcemaps.write() : $.util.noop())
    .pipe(config.minify ? $.cssnano({
      autoprefixer: false,
      normalizeUrl: false
    }) : $.util.noop())
    .pipe($.rename('main.css.liquid'))
    .pipe($.replace(/background: url\((.*?)\)/g, replaceWithAssetURL))
    .pipe($.replace('"{', '{'))
    .pipe($.replace('}"', '}'))
    .pipe(gulp.dest('public/assets'))
    .pipe(reload({
      stream: true,
      once: true
    }))
})
