import gulp from 'gulp'
import plugins from 'gulp-load-plugins'
import config from '../config/assets/default'
import browserSync from 'browser-sync'
import { union } from 'lodash'

const reload = browserSync.reload
const $ = plugins()

gulp.task('images', () => {
  return gulp.src(union(config.client.images, config.client.lib.images))
    .pipe($.newer('./public/assets/images'))
    .pipe($.imagemin({
      optimizationLevel: 3,
      progressive: true,
      interlaced: true
    }))
    .pipe($.flatten())
    .pipe(gulp.dest('./public/assets'))
    .pipe(reload({
      stream: true,
      once: true
    }))
})
