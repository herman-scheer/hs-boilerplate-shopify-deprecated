import gulp from 'gulp'
import plugins from 'gulp-load-plugins'
import config from '../config/assets/default'
import browserSync from 'browser-sync'

const $ = plugins()

gulp.task('icons-color', () => {
  return gulp.src(config.client.icons.color)
    .pipe($.svgmin())
    .pipe($.svgSprite({
      mode: {
        symbol: {
          dest: '',
          sprite: 'color.svg.liquid'
        }
      },
      shape: {
        id: {
          separator: '-',
          generator: (name) => {
            const newName = name.replace('core/icons/color/', '').replace('.svg', '').replace(/\//g, '-')
            return newName
          }
        }
      }
    }))
    .pipe(gulp.dest('public/snippets'))
    .pipe(browserSync.reload({
      stream: true,
      once: true
    }))
})

gulp.task('icons-no-color', () => {
  return gulp.src(config.client.icons.noColor)
    .pipe($.svgmin())
    .pipe($.svgSprite({
      mode: {
        symbol: {
          dest: '',
          sprite: 'no-color.svg.liquid'
        }
      },
      shape: {
        id: {
          separator: '-',
          generator: (name) => {
            const newName = name.replace('core/icons/no-color/', '').replace('.svg', '').replace(/\//g, '-')
            return newName
          }
        }
      }
    }))
    .pipe($.cheerio({
      run: function run(jquery) {
        jquery('[fill]').removeAttr('fill')
      },
      parserOptions: {
        xmlMode: true
      }
    }))
    .pipe(gulp.dest('public/snippets'))
    .pipe(browserSync.reload({
      stream: true,
      once: true
    }))
})

gulp.task('icons', ['icons-no-color', 'icons-color'])
